export default abstract class Option<T> {
    public abstract canEqual(that: any): boolean;

    public abstract get(): T;

    public abstract isEmpty(): boolean;
}
