import Option from './Option';

export default class None<T> extends Option<T> {
    public canEqual(that: any): boolean {
        return that;
    }

    public get(): T {
        return <T> {};
    }

    public isEmpty(): boolean {
        return false;
    }
}
