import Option from '../option/Option';

/**
 * The {@link Try} class represents a computation that may either result in an exception,
 * or return a successfully computed value.
 *
 * It's similar to, but semantically different from the {@link Either} class.
 *
 * Instances of {@link Try}<T>, are either an instance of {@link Success}<T> or {@link Failure}<T>.
 *
 * For example, {@link Try} can be used to perform merging of stringified arrays,
 * without the need to do explicit exception-handling in all of the places that an exception might occur.
 *
 * @example
 * ```typescript
 *
 * import { Try } from '@monads/monads';
 *
 * function parseAndMergeJsonArray(arr1: string, arr2: string): Try<Array<any>> {
 *    const parsedArr1 = Try.invoke<Array<any>>(() => JSON.parse(arr1))
 *                        .filter(a => a instanceof Array);
 *    const parsedArr2 = Try.invoke<Array<any>>(() => JSON.parse(arr2))
 *                        .filter(b => b instanceof Array);
 *    return parsedArr1.flatMap(a => parsedArr2.map(b => a.concat(b)));
 * }
 * ```
 *
 * @typeparam T Type of the returned value.
 */
export default abstract class Try<T> {
    /**
     * Static method used as entrypoint for {@link Try}.
     *
     * @param mapper
     */
    public static invoke<U>(mapper: () => U): Try<U> {
        try {
            const value = mapper();
            return new (require('./Success').default)(value);
        } catch (e) {
            return new (require('./Failure').default)(e);
        }
    }

    /**
     * Returns the value of the {@link Try} if it is a {@link Success},
     * and throws an exception if it is a {@link Failure}.
     */
    public abstract get value(): T;

    /**
     * Returns whether a {@link Try} is a {@link Success} (true) or a {@link Failure} (false).
     */
    public abstract get isSuccess(): boolean;

    /**
     * Returns whether a {@link Try} is a {@link Failure} (true) or a {@link Success} (false).
     */
    public abstract get isFailure(): boolean

    /**
     * Gets the value of the {@link Try} if it is a {@link Success}, and throws an exception if it is a {@link Failure}.
     */
    public abstract get(): T;

    /**
     * Returns the value of the {@link Try} if it is a {@link Success},
     * and the default value of T if it is a {@link Failure}.
     */
    public abstract getOrDefault(): T;

    /**
     * Maps an instance of type T to an instance of type U by invoking function mapper.
     *
     * @param mapper
     */
    public abstract map<U>(mapper: (v: T) => U): Try<U>;

    /**
     * Maps an instance of type T to an instance of type {@link Try} by invoking the function mapper.
     *
     * @param mapper
     */
    public abstract flatMap<U>(mapper: (v: T) => Try<U>): Try<U>;

    /**
     * Recover from a {@link Failure} holding any exception to a {@link Try}.
     *
     * @param recover
     */
    public abstract recover(recover: (e: Error) => T): Try<T>;

    /**
     * Filter the value.
     *
     * @param filter
     */
    public abstract filter(filter: (v: T) => any): Try<T>;

    /**
     * Can set value on an object and return it.
     */
    public abstract set(setter: (v: T) => any): Try<T>;

    /**
     * Returns the value of the {@link Try} if it is a {@link Success},
     * or the other parameter if it is a {@link Failure}.
     *
     * @param other
     */
    public getOrElse(other: T): T {
        if (this.isFailure) {
            return other;
        }
        return this.value;
    }

    /**
     * Returns the value of the {@link Try} if it is a {@link Success},
     * or the result of the other parameter if it is a {@link Failure}.
     *
     * @param other
     */
    public orElse(other: () => T): Try<T> {
        if (this.isFailure) {
            return Try.invoke<T>(other);
        }
        return this;
    }

    public toOption(): Option<T> {
        return <any> {};
    }
}
