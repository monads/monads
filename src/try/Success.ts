import Failure from './Failure';
import Try from './Try';

/**
 * @private
 */
export default class Success<T> extends Try<T> {
    private tValue: T;

    public constructor(value: T) {
        super();
        this.tValue = value;
    }

    public get value(): T {
        return this.tValue;
    }

    public get isSuccess(): boolean {
        return true;
    }

    public get isFailure(): boolean {
        return false;
    }

    public get(): T {
        return this.value;
    }

    public getOrDefault(): T {
        return this.value;
    }

    public map<U>(mapper: (v: T) => U): Try<U> {
        return Try.invoke<U>(() => mapper(this.value));
    }

    public flatMap<U>(mapper: (v: T) => Try<U>): Try<U> {
        return Try.invoke<U>(() => mapper(this.value).get());
    }

    public recover(recover: (e: Error) => T): Try<T> {
        return this;
    }

    public filter(filter: (v: T) => any): Try<T> {
        try {
            const result = filter(this.value);
            if (result) {
                return this;
            }
            return new Failure<T>('There are no results from applying the filter');
        } catch (e) {
            return new Failure<T>('There are no results from applying the filter');
        }
    }

    public set(setter: (v: T) => any): Try<T> {
        try {
            setter(this.value);
            return this;
        } catch (e) {
            return new Failure<T>(e);
        }
    }
}
