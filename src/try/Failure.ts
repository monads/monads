import Success from './Success';
import Try from './Try';

/**
 * @private
 */
export default class Failure<T> extends Try<T> {
    private tValue: Error;

    public constructor(value: Error | string) {
        super();
        if (typeof value === 'string') {
            value = new Error(value);
        }
        this.tValue = value;
    }

    public get value(): any {
        return this.tValue;
    }

    public get isSuccess(): boolean {
        return false;
    }

    public get isFailure(): boolean {
        return true;
    }

    public get(): T {
        throw this.value;
    }

    public getOrDefault(): T {
        // if(this._class && this._class.default && typeof this._class.default === 'function') {
        //     return this._class.default();
        // }
        throw this.value;
    }

    public map<U>(mapper: (v: T) => U): Try<U> {
        return new Failure<U>(this.value);
    }

    public flatMap<U>(mapper: (v: T) => Try<U>): Try<U> {
        return new Failure<U>(this.value);
    }

    public recover(recover: (e: Error) => T): Try<T> {
        try {
            return new Success<T>(recover(this.value));
        } catch (e) {
            return new Failure<T>(this.value);
        }
    }

    public filter(filter: (v: T) => any): Try<T> {
        return this;
    }

    public set(setter: (v: T) => any): Try<T> {
        return this;
    }
}
