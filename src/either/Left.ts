import Either from './Either';

export default class Left<T, U> extends Either<T, U> {
    protected tValue: T;

    public get value(): T {
        return this.tValue;
    }

    public get isLeft(): boolean {
        return true;
    }

    public get isRight(): boolean {
        return false;
    }

    constructor(value: T) {
        super();
        this.tValue = value;
    }

    public toString() {
        return `Left(${this.value})`;
    }
}
