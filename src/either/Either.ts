/**
 * It represent a discriminated union of two possible types.
 *
 * Instances of {@link Either} are either an instance of {@link Left} or {@link Right}.
 *
 * A common use of {@link Either} is as an alternative to {@link Option} for dealing with possible missing values.
 *
 * @example
 * ```typescript
 *
 * function parseObject(v: string) {
 *     return Either.attempt<string, any>(() => JSON.parse(v));
 * }
 *
 * console.log(parseObject('{x:1}').fold(
 *     v => `Sorry but this is not a JSON object '${v}'`,
 *     v => `This is a JSON object '${v}'`
 * ));
 * ```
 *
 * @typeparam T Type of {@link Left} item.
 * @typeparam U Type of {@link Right} item.
 */
export default abstract class Either<T, U> {
    /**
     * Attempt the given mapper, will return either {@link Left} when failed or {@link Right} when succeeded.
     *
     * @param mapper
     * @param value
     * @typeparam T Type of {@link Left} item.
     * @typeparam U Type of {@link Right} item.
     */
    public static attempt<T, U>(mapper: () => U): Either<T, U>;
    public static attempt<T, U>(mapper: (expectedValue: any) => U, givenValue: any): Either<T, U>;
    public static attempt<T, U>(mapper: any, givenValue?: any): Either<T, U> {
        try {
            return new (require('./Right').default)(mapper(givenValue));
        } catch (e) {
            return new (require('./Left').default)(e);
        }
    }

    /**
     * Contains the value of the {@link Either}, either {@typeparam T} or U.
     */
    public abstract value: T | U;

    /**
     * Returns true if this is a {@link Left}, false otherwise.
     */
    public abstract isLeft: boolean;

    /**
     * Returns true if this is a {@link Right}, false otherwise.
     */
    public abstract isRight: boolean;

    /**
     * Applies left if this is a {@link Left} or right if this is a {@link Right}.
     *
     * @param left
     * @param right
     */
    public fold<X>(left: (v: T) => X, right: (v: U) => X): X {
        if (this.isLeft) {
            return left(<T> this.value);
        }
        return right(<U> this.value);
    }

    /**
     * If this is a {@link Left}, then return the left value in {@link Right} or vice versa.
     */
    public swap(): Either<T, U> {
        if (this.isLeft) {
            return new (require('./Right').default)(<U> this.value);
        }
        return new (require('./Left').default)(<T> this.value);
    }

    /**
     * Creates a string representation of this object.
     */
    public abstract toString(): string;
}
