import Either from './Either';

export default class Right<T, U> extends Either<T, U> {
    protected tValue: U;

    public get value(): U {
        return this.tValue;
    }

    public get isLeft(): boolean {
        return false;
    }

    public get isRight(): boolean {
        return true;
    }

    constructor(value: U) {
        super();
        this.tValue = value;
    }

    public toString() {
        return `Right(${this.value})`;
    }
}
