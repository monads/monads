import Try from '../../src/try/Try';
import UserFactory from './UserFactory';
import User from './User';

const userFactory = new UserFactory();

describe('class Try', () => {
    describe('.isSuccess()', () => {
        it('will return true', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'));

            expect(result.isSuccess).toEqual(true);
        });

        it('will return false when an Error is thrown', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                            .map(u => u.throwError());

            expect(result.isSuccess).toEqual(false);
        });
    });

    describe('.isFailure()', () => {
        it('will return true', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                            .map(u => u.throwError());

            expect(result.isFailure).toEqual(true);
        });

        it('will return false when no Error is thrown', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'));

            expect(result.isFailure).toEqual(false);
        });
    });

    describe('.get()', () => {
        it('will return a User', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .get();

            expect(result.name).toEqual('John');
        });

        it('will throw an Error when failed', () => {
            // This is ironic tbh.
            try {
                let result = Try.invoke<User>(() => userFactory.create('John'))
                                .map(e => e.throwError())
                                .get();
            } catch(e) {
                expect(e).toBeInstanceOf(Error);
            }
        });
    });

    describe('.getOrDefault()', () => {
        it('will return a User', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .getOrDefault();

            expect(result.name).toEqual('John');
        });

        // TODO: default
        // it('will return a User when an Error is thrown', async () => {
        //     let result = await Try.invoke<User>(() => userFactory.create('John'))
        //                     .map(e => e.throwError())
        //                     .getOrDefault();

        //     expect(result.name).toEqual('Jack');
        // });

        it('will throw an Error when no default has been found', () => {
            // Still ironic
            try {
                let result = Try.invoke<User>(() => userFactory.createWithoutDefault('John'))
                                    .map(e => e.throwError())
                                    .getOrDefault();
            } catch(e) {
                expect(e).toBeInstanceOf(Error);
            }
        });
    });

    describe('.getOrElse()', () => {
        it('will return a User named John', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .getOrElse(userFactory.create('Jack'));

            expect(result.name).toEqual('John');
        });

        it('will return a User named Jack when an Error is thrown', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .map(e => e.throwError())
                                    .getOrElse(userFactory.create('Jack'));

            expect(result.name).toEqual('Jack');
        });
    });

    describe('.orElse()', () => {
        it('will return a User named John', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .orElse(() => userFactory.create('Jack'))
                                    .get();

            expect(result.name).toEqual('John');
        });

        it('will return a User named Jack when an filter fails or Error is thrown', () => {
            let result = Try.invoke<User>(() => userFactory.create('Jack'))
                                    .filter(e => e.isNameJohn().get())
                                    .orElse(() => userFactory.create('Jack'))
                                    .get();

            expect(result.name).toEqual('Jack');
        });
    });

    describe('.map()', () => {
        it('will return a boolean', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .map<string>(e => e.name)
                                    .map<boolean>(s => s.endsWith('n'))
                                    .get();

            expect(result).toEqual(true);
        });

        it('will return a Failure when an Error is thrown', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .map(e => e.throwError())
                                    .map<string>(e => e.name);

            expect(result.isFailure).toEqual(true);
        });
    });

    describe('.flatMap()', () => {
        it('will return a boolean', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .flatMap<boolean>(e => e.isNameJohn())
                                    .get();

            expect(result).toEqual(true);
        });

        it('will return a Failure when an Error is thrown', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .flatMap<User>(e => e.throwError())
                                    .flatMap<boolean>(e => e.isNameJohn());

            expect(result.isFailure).toEqual(true);
        });
    });

    describe('.recover()', () => {
        it('will return a User named John', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .recover(err => userFactory.create('Jack'));

            expect(result.isSuccess).toEqual(true);
            expect(result.get().name).toEqual('John');
        });

        it('will return a User named Jack when an Error is thrown', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .flatMap<User>(e => e.throwError())
                                    .recover(err => userFactory.create('Jack'));

            expect(result.isSuccess).toEqual(true);
            expect(result.get().name).toEqual('Jack');
        });

        it('will return a Failure when the recover also fails', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .flatMap<User>(e => e.throwError())
                                    .recover(err => userFactory.create('Jack').throwError());

            expect(result.isFailure).toEqual(true);
        });
    });

    describe('.filter()', () => {
        it('will return Success', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .filter(e => e.isNameJohn().get());

            expect(result.isSuccess).toEqual(true);
        });

        it('will return a Failure when the filter returns falsey', () => {
            let result = Try.invoke<User>(() => userFactory.create('Jack'))
                                    .filter(e => e.isNameJohn().get());

            expect(result.isFailure).toEqual(true);
        });

        it('will return a Failure when an Error is thrown', () => {
            let result = Try.invoke<User>(() => userFactory.create('Jack'))
                                    .filter(e => e.throwError())
                                    .filter(e => e.isNameJohn().get());

            expect(result.isFailure).toEqual(true);
        });
    });

    describe('.set()', () => {
        it('will return a User named Jack', () => {
            let age = new Date("07/12/1990");
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .set(e => e.name = "Jack")
                                    .set(e => e.age = age)
                                    .get();

            expect(result.name).toEqual('Jack');
            expect(result.age).toEqual(age);
        });

        it('will return a Failure when an Error is thrown', () => {
            let result = Try.invoke<User>(() => userFactory.create('John'))
                                    .set(e => e.throwError())
                                    .set(e => e.name = "Jack");

            expect(result.isFailure).toEqual(true);
        });
    });
});
