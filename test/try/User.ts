import Try from "../../src/try/Try";

export default class User {
    /**
     * The name of the User.
     */
    public name: string;

    /**
     * The age of the User.
     */
    public age: Date;

    /**
     * Simple user for testing.
     * 
     * @param name 
     */
    public constructor(name: string) {
        this.name = name;
    }

    /**
     * Returns Try to use in flatMap.
     */
    public isNameJohn(): Try<boolean> {
        return Try.invoke<boolean>(() => this.name === 'John');
    }

    /**
     * Throw an Error for testing.
     * 
     * Return type is any, for the sake of testing, normally
     * you wouldn't know that it would throw an error.
     */
    public throwError(): any {
        throw new Error('Thrown error for tests');
    }
}

export class UserWithDefault extends User {
    /**
     * This is the default function that developers can use.
     */
    public static default() {
        return new User('Jack');
    }
}