import User, { UserWithDefault } from "./User";

export default class UserFactory {
    /**
     * Simple factory to create users.
     * 
     * @param name 
     */
    public create(name: string) {
        return new UserWithDefault(name);
    }

    /**
     * Create user without the default method implemented.
     * 
     * @param name 
     */
    public createWithoutDefault(name: string) {
        return new User(name);
    }
}