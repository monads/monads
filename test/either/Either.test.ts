import Either from '../../src/either/Either';

const returnIfLowerThenOne = (value: number) => {
    if (value < 1) {
        return value;
    }
    throw new Error(`${value} is not lower then 1`);
}

describe('Either', () => {
    describe('.value', () => {
        it('will return the error if it fails', () => {
            const value = 1.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower.value).toBeInstanceOf(Error);
        });

        it('will return the value if it succeeds', () => {
            const value = 0.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower.value).toEqual(value);
        });
    });

    describe('.isLeft', () => {
        it('will return true if it fails', () => {
            const value = 1.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower.isLeft).toEqual(true);
        });

        it('will return false if it fails', () => {
            const value = 0.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower.isLeft).toEqual(false);
        });
    });

    describe('.isRight', () => {
        it('will return true if it fails', () => {
            const value = 1.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower.isRight).toEqual(false);
        });

        it('will return false if it fails', () => {
            const value = 0.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower.isRight).toEqual(true);
        });
    });

    describe('.fold()', () => {
        it('will return the value if it succeeded', (done) => {
            const value = 0.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            eitherLower.fold(l => {
                expect(l).toEqual(value);
                done();
            }, r => {
                expect(r).toEqual(value);
                done();
            });
        });

        it('will return the error if it failed', (done) => {
            const value = 1.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            eitherLower.fold(l => {
                expect(l).toBeInstanceOf(Error);
                done();
            }, r => {
                expect(r).toBeInstanceOf(Error);
                done();
            });
        });
    });

    describe('.swap()', () => {
        it('will return the value if it succeeded', () => {
            const value = 0.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower.isLeft).toEqual(false);
            expect(eitherLower.swap().isLeft).toEqual(true);
        });

        it('will return the error if it failed', () => {
            const value = 1.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower.isLeft).toEqual(true);
            expect(eitherLower.swap().isLeft).toEqual(false);
        });
    });

    describe('.toString()', () => {
        it('will return "Left(error)" if it fails', () => {
            const value = 1.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower + "").toEqual("Left(Error: 1.5 is not lower then 1)");
            expect(eitherLower.toString()).toEqual("Left(Error: 1.5 is not lower then 1)");
        });

        it('will return "Right(value)" if it succeeds', () => {
            const value = 0.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower + "").toEqual("Right(0.5)");
            expect(eitherLower.toString()).toEqual("Right(0.5)");
        });
    });

    describe('::attempt()', () => {
        it('will attempt to execute the given function', () => {
            const value = 0.5;
            const eitherLower = Either.attempt<Error, Number>(returnIfLowerThenOne, value);

            expect(eitherLower).toBeInstanceOf(Either);
            expect(eitherLower.isLeft).toEqual(false);
        });
    });
});
