import * as monads from '../src/index';
import Try from '../src/try/Try';
import Either from '../src/either/Either';

describe('index.ts', () => {
    it('will contain the Try monad', async () => {
        expect(monads.Try).toEqual(Try);
    });

    it('will contain the Either monad', async () => {
        expect(monads.Either).toEqual(Either);
    });
});
