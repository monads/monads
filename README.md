[![npm version](https://img.shields.io/npm/v/monads.svg)](https://www.npmjs.com/package/monads) 
[![npm downloads](https://img.shields.io/npm/dm/monads.svg)](https://www.npmjs.com/package/monads) 
[![pipeline status](https://gitlab.com/monads/monads/badges/master/pipeline.svg)](https://gitlab.com/monads/monads/pipelines) 
[![coverage report](https://gitlab.com/monads/monads/badges/master/coverage.svg)](https://monads.gitlab.io/coverage)

# Monads
You already got Javascript most famous monad, the Promise. But why stop there?

## Download
Monads is released under the [MIT License](LICENSE) and supports modern environments.

## Installation
In a browser:
```html
<script src="monads.js"></script>
```

Using npm:
```shell
$ npm i --save @monads/monads
```

In Node.js:
```typescript
// Load the full package
const monads = require('@monads/monads');

// Load a specific package.
const { Try } = require('@monads/monads');
```

## Running the tests
To run all the tests, install the dependencies and then run `npm run test`:
```shell
$ npm install
$ npm run test
```

## Want to help?
[test](CONTRIBUTING)

## Contributers
[Jochum van der Ploeg](https://gitlab.com/wolfenrain)

## License
[MIT](https://gitlab.com/monads/monads/blob/master/LICENSE)