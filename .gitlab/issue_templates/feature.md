# Feature Request

#### Monads version:
- [x] 1.x.x

#### Describe the Feature Request:
<!-- A good description of what the feature request is, helps alot -->

#### Describe preferred solution:
<!-- A good description of what you want to happen -->

#### Describe alternatives:
<!-- A good description of any other solutions that you've considered -->

#### Related code:
<!-- If you are able to explain the feature request using examples, please provide such -->
```typescript

```

#### Additional context:
<!-- Put leftover information here -->

/label ~feature