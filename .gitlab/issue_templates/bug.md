# Bug Report

#### Monads version:
- [x] 1.x.x

#### Current behavior:
<!-- Describe how the bug manifests -->

#### Expected behavior:
<!-- Describe what you expected to happen -->

#### Steps to reproduce:
<!-- Put steps to reproduce here -->
- ??

#### Related code:
<!-- If you are able to explain the bug report using examples, please provide such -->
```typescript

```

#### Other information:
<!-- Put leftover information here -->

/label ~bug